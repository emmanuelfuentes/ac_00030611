org 	100h;

;EJERCICIO 1 
	

	;carnet 00030611 

	;guardando notas en celdas 210 hasta 214

	mov al, 3d
	mov ah, 0d
	mov bl, 6d
	mov bh, 1d
	mov cl, 1d

	mov [210h], al
	mov [211h], ah
	mov [212h], bl
	mov [213h], bh
	mov [214h], cl


	mov	ax, 0d
	mov	bx, 0d
	mov	cx, 0d
	mov	dx, 0d

	mov	ax, 5d	;condicion de paro
	mov	bx, 0d	;bandera  desplazamiento
	mov	cx, 0d	;guardar	
	mov 	dx, 0d	;acumulador

suma:	mov 	cx, [210h + bx] 
	add 	dl,cl		
				
	inc 	bx		
	cmp	ax,bx
	ja 	suma

	mov	[27Ah], dl ;Resultado de la suma 
	mov	ax, 0000h
	
	mov	al, dl
	
	DIV 	bl

	mov	[27Bh],al ;aca pongo el resultado del promedio

;resultado de promedio 2 
	
	mov word [201h], "AU"
	mov word [203h], "N"

	mov word [205h], "SE"

	mov word [208h], "PA"

	mov word [20Ah], "SA"



 ;Ejercicio 2
	mov     ax, 4h	
	mov		bx, 0d	;idx	

	;menor de 100h

covid1:	mov 	[210h + bx], ax	
		mov 	cx,2h	;
		mul	cx	
		inc	bx
		cmp	ax, 100h ;comparacion
		jb	covid1  ;llamada funcion 1

	;mayor que 100h

covid2:	mov 	[210h + bx], ax
	mov 	cx,2h
	mul	cx
	inc	bx 
	inc	bx
	cmp	ax, 4097d ;si noe s mayor no escribe
	jb	covid2

;ejercicio 3

	mov	ax, 0000h	;variable aux
	mov	bx, 0000h	;indice
	mov	cx, 0000h	;caso base
	mov	dx, 0000h	;caso base2

	mov	bl, 2d	;indice = 2
	mov 	dl, 1d 	;F[1] = 1
	mov	[220h], cl	
	mov	[221h], dl	

fibonacci:	mov	ax, dx	;AX = F[n-1]
	add	ax, cx	;AX += F[n-2]
	mov	[220h+bx], ax 	
	mov	cx, dx	;F[n-2] = F[n-1]
	mov	dx, ax 	;F[n-1] = AX
	inc 	bl 	;incrementando bl

	cmp	bl, 14d	
	jb	fibonacci	;llamada a funcion creada anteriormente



;caso especial para 255

	mov	ax, dx	
	add	ax, cx	
	mov	[220h+bx], ah
	inc 	bl 	
	mov	[220h+bx], al
	mov	cx, dx	
	mov	dx, ax 


	int 20h